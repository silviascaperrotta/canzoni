package it.begear.springTopBoot.entities;

import javax.persistence.*;

@Entity  //Questa classe corrisponde a una tabella
@Table(name="canzoni") //corrisponde al nome della tabella (entità)
public class Canzone {
	
	@Id  // Obbligatorio
	@GeneratedValue(strategy = GenerationType.IDENTITY)  //ID incrementale su tutta la tabella
	@Column(name = "id")  //corrisponde al nome della colonna (attributo)
	private Long id;
	
	@Column(name = "titolo")
	private String titolo;
	
	public int getAnno() {
		return anno;
	}

	public void setAnno(int anno) {
		this.anno = anno;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	@Column(name = "anno")
	private int anno;
	
	@Column(name = "album")
	private String album;
	


	public Canzone() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitolo() {
		return titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}


}
