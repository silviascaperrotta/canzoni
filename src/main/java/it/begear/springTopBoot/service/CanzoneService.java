package it.begear.springTopBoot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.begear.springTopBoot.entities.Canzone;
import it.begear.springTopBoot.repository.CanzoneRepo;

@Service
public class CanzoneService {
 
    @Autowired
    private CanzoneRepo repo;
     
    public List<Canzone> listAll() {
        List<Canzone> lista = repo.findAll();
        return lista;
    }
     
    public void save(Canzone product) {
        repo.save(product);
    }
     
    public Canzone get(long id) {
        return repo.findById(id).get();
    }
     
    public void delete(long id) {
        repo.deleteById(id);
    }
    
    public List<Canzone> findByTitolo(String titolo) {
    	return repo.findCanzoneByTitolo(titolo);
    }
}

