package it.begear.springTopBoot.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import it.begear.springTopBoot.entities.Canzone;
import it.begear.springTopBoot.service.CanzoneService;

@Controller
public class CanzoneController {
	
	@Autowired
    CanzoneService canzoneService;
	
	@RequestMapping("/")
	public String viewHomePage(Model model) {
	    List<Canzone> listaCanz = canzoneService.listAll();
	    model.addAttribute("listaCanz",listaCanz);
	    return "index";
	}
	
	@RequestMapping("/new")
	public String showNewProductPage(Model model) {
	    Canzone canz = new Canzone();
	    model.addAttribute("canz", canz);
	    return "add-canz";
	}
    
    @RequestMapping("/addCanz")
    public String saveProduct(@ModelAttribute("canz") Canzone canz) {
        canzoneService.save(canz);
        return "redirect:/";
    }
    
    @RequestMapping("/edit/{id}")									
    public ModelAndView showEditCanz(@PathVariable(name = "id") int id) {
        ModelAndView mav = new ModelAndView("edit-canz");
        Canzone canz = canzoneService.get(id);
        mav.addObject("canz", canz);
        return mav;
    }
    
    @RequestMapping("/delete/{id}")
    public String deleteProduct(@PathVariable(name = "id") int id) {
        canzoneService.delete(id);
        return "redirect:/";       
    }
    
    
    @RequestMapping("/search")
    public String findByTitolo(@RequestParam("titolo") String titolo, Model model) {
    	List<Canzone> listaCanz = canzoneService.findByTitolo(titolo);
        model.addAttribute("listaCanz", listaCanz);
        return "index";
    }
     
  

}
